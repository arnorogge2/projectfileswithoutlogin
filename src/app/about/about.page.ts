import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Http } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage'
@Component({
  selector: 'app-about',
  templateUrl: 'about.page.html',
  styleUrls: ['about.page.scss']
})
export class AboutPage {
  datainfo: any;
  datasim: any;
  type: any;
  id: any;
  constructor(public navCtrl: NavController,  public http: Http, public route: ActivatedRoute, public rtr: Router, private storage: Storage) {


  }

  ngOnInit() {
    var idDisplay;
    var typeDisplay;

    this.storage.get('idDisplay').then((val) => {
      if (val != null) {
        idDisplay = val.toString();
        this.id = idDisplay;
      } else {

      }
    });
    this.storage.get('typeDisplay').then((val) => {
      if (val != null) {
        typeDisplay = val;
        if (typeDisplay == "Serie") {
          this.displaySerie(idDisplay);
        }
        else if (typeDisplay == "Movie") {
          this.displayMovie(idDisplay);
        }
        else {
          this.displayPerson(idDisplay);
        }
      } else {

      }
    });
  }

  displaySerie(idDisplay) {
    this.http.get('https://api.themoviedb.org/3/tv/' + idDisplay + '?api_key=37ccb790eeea88e5d24a68672814040a&language=en-US').map(res => res.json()).subscribe(data => {
      this.type = "Serie";
      this.datainfo = data;
      this.datainfo.first_air_date = this.datainfo.first_air_date.substring(0, 4);

    }, err => {
      console.log(err);
    });
    this.http.get('https://api.themoviedb.org/3/tv/' + idDisplay + '/similar?api_key=37ccb790eeea88e5d24a68672814040a&language=en-US&page=1').map(res => res.json())
      .subscribe(data => {
        this.datasim = data.results;
      }, err => {
        console.log(err);
      });
  }
  displayMovie(idDisplay) {
    this.http.get('https://api.themoviedb.org/3/movie/' + idDisplay + '?api_key=37ccb790eeea88e5d24a68672814040a&language=en-US').map(res => res.json()).subscribe(data => {
      this.type = "Movie";
      this.datainfo = data;
      this.datainfo.release_date = this.datainfo.release_date.substring(0, 4);
    }, err => {
      console.log(err);
    });
    this.http.get('https://api.themoviedb.org/3/movie/' + idDisplay + '/similar?api_key=37ccb790eeea88e5d24a68672814040a&language=en-US&page=1').map(res => res.json())
      .subscribe(data => {
        this.datasim = data.results;
      }, err => {
        console.log(err);
      });
  }
  displayPerson(idDisplay) {
    this.http.get('https://api.themoviedb.org/3/person/' + idDisplay + '?api_key=37ccb790eeea88e5d24a68672814040a&language=en-US').map(res => res.json()).subscribe(data => {
      this.type = "Person";
      this.datainfo = data;
      this.datainfo.release_date = this.datainfo.release_date.substring(0, 4);
    }, err => {
      console.log(err);
    });
  }


  follow() { 
    event.stopPropagation();
    var followed = "";
    if(this.type == "Serie"){
    this.storage.get('followedSeries').then((val) => {
      if (val != null) {
        followed = val.toString();
        if (followed.includes(this.id)){

        } else{
          followed = followed + "," + this.id;
          this.storage.set('followedSeries', followed);
          console.log(followed);
          window.location.reload();
        }      
      } else {
        followed = this.id;
        this.storage.set('followedSeries', followed);
        window.location.reload();
      }
    });
  }else{
    this.storage.get('followedMovies').then((val) => {
      if (val != null) {
        followed = val.toString();
        if (followed.includes(this.id)){

        } else{
          followed = followed + "," + this.id;
          this.storage.set('followedMovies', followed);
          console.log(followed);
          window.location.reload();
        }      
      } else {
        followed = this.id;
        this.storage.set('followedMovies', followed);
        window.location.reload();
      }
    });
  }
  }

  clickedSearch() {
    this.rtr.navigateByUrl('/tabs/(information:information)');
  }
}
