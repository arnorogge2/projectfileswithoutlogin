import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TabsPage } from './tabs.page';
import { HomePage } from '../home/home.page';
import { AboutPage } from '../about/about.page';
import { ContactPage } from '../contact/contact.page';
import { UpcomingPage } from '../upcoming/upcoming.page';
import { InformationPage } from '../information/information.page';
import { ProfilePage } from '../profile/profile.page';
import { LoginPage } from '../login/login.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: '',
        redirectTo: '/tabs/(upcoming:upcoming)',
        pathMatch: 'full',
      },
      {
        path: 'home',
        outlet: 'home',
        component: HomePage
      },
      {
        path: 'about',
        outlet: 'about',
        component: AboutPage
      },
      {
        path: 'contact',
        outlet: 'contact',
        component: ContactPage
      },
      {
        path: 'upcoming',
        outlet: 'upcoming',
        component: UpcomingPage
      },
      {
        path: 'information',
        outlet: 'information',
        component: InformationPage
      },
      {
        path: 'profile',
        outlet: 'profile',
        component: ProfilePage
      },
      {
        path: 'login',
        outlet: 'login',
        component: LoginPage
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/(upcoming:upcoming)',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
