import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage'

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  followedSeries: any;
  followedSeriedArray: any;
  followedMovies: any;
  followedMoviedArray: any;
  foundSeries: any;
  foundMovies: any;

  constructor(public http: Http, public rtr: Router, private storage: Storage) {
  }

  ngOnInit() {
    var followedSeriedArray = [];
    var followedMoviedArray = [];
    this.storage.get('followedSeries').then((val) => {
      if (val != null) {
        this.followedSeries = (val.toString()).split(',');
        var i;
        for (i = 0; i < this.followedSeries.length; i++) {
          if (this.followedSeries[i] != "") {
            this.http.get('https://api.themoviedb.org/3/tv/' + this.followedSeries[i] + '?api_key=37ccb790eeea88e5d24a68672814040a&language=en-US').map(res => res.json()).subscribe(data => {
              followedSeriedArray.push(data);
              this.followedSeriedArray = followedSeriedArray;
            }, err => {
              console.log(err);
            });
          }
        }
        this.foundSeries = true;
      } else {  
      }
    });
    this.storage.get('followedMovies').then((val) => {
      if (val != null) {
        this.followedMovies = (val.toString()).split(',');
        var i;
        for (i = 0; i < this.followedMovies.length; i++) {
          if (this.followedMovies[i] != "") {
            this.http.get('https://api.themoviedb.org/3/movie/' + this.followedMovies[i] + '?api_key=37ccb790eeea88e5d24a68672814040a&language=en-US').map(res => res.json()).subscribe(data => {
              followedMoviedArray.push(data);
              this.followedMoviedArray = followedMoviedArray;
            }, err => {
              console.log(err);
            });
          }
        }
        this.foundMovies = true;
      } else {  
      }
    });


  }
}
