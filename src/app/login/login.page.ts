import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';
import { Router } from '@angular/router';
import { NavController, Tabs} from '@ionic/angular';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireMessagingModule } from '@angular/fire/messaging';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loggedIn: any;
  constructor(public fdb: AngularFireDatabase, public afAuth: AngularFireAuth, public rtr: Router) { 
    
  }

  writeDatabase(newUser){
     this.fdb.list("/users/").set(newUser.uid,{
      displayName: newUser.displayName,
      email: newUser.email,
      movies: ',',
      series: ',',
    });  
  }
  Facebooklogin() {
    /* this.afAuth.auth.signInWithPopup(new auth.FacebookAuthProvider()).then((result)=>{ */
     this.afAuth.auth.signInWithRedirect(new auth.FacebookAuthProvider());
     this.afAuth.auth.getRedirectResult().then((result)=>{
      if (result.additionalUserInfo.isNewUser){    
        var newUser = result.user; 
        this.writeDatabase(newUser); 
      }    
    });    
    this.rtr.navigateByUrl('/tabs/(upcoming:upcoming)');
  }
  Googlelogin() {
     this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider()).then((result)=>{
      if (result.additionalUserInfo.isNewUser){    
        var newUser = result.user; 
        this.writeDatabase(newUser); 
      }    
    });     
    this.rtr.navigateByUrl('/tabs/(upcoming:upcoming)');
  }

  AlreadyLoggedIn(){
    this.rtr.navigateByUrl('/tabs/(upcoming:upcoming)');
  }

  ngOnInit() {
    this.afAuth.auth.onAuthStateChanged(user => {
      if (user) {
        this.loggedIn = "loggedin";
      }
    });
  }

}
