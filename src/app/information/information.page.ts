import { Component, OnInit, ViewChild } from '@angular/core';
import { IonicSelectableComponent } from 'ionic-selectable';
import { NavController } from '@ionic/angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { Router } from '@angular/router';
import { Storage } from '@ionic/storage'

@Component({
  selector: 'app-information',
  templateUrl: './information.page.html',
  styleUrls: ['./information.page.scss'],
})
export class InformationPage implements OnInit {

  @ViewChild('myselect') selectComponent: IonicSelectableComponent;
  arrData = []
  data: string;
  newArray: any;
  newArrayLoad: any;
  target: any;
  number: any;
  constructor(public navCtrl: NavController, public http: Http, public rtr: Router, private storage: Storage) {

  }

  searchChanged(ev: any) {
    if (ev.target.value == "") {
      this.target = "a";
    } else {
      this.target = ev.target.value;
    }
    this.http.get('https://api.themoviedb.org/3/search/multi?api_key=37ccb790eeea88e5d24a68672814040a&language=en-US&query=' + this.target + '&page=1&include_adult=false').map(res => res.json())
      .subscribe(data => {
        this.data = data.results;
        var old = JSON.stringify(this.data).replace(/original_title/g, "display");
        var old = old.replace(/original_name/g, "display");
        var old = old.replace(/name/g, "display");
        var old = old.replace(/tv/g, "Serie");
        var old = old.replace(/movie/g, "Movie");
        var old = old.replace(/person/g, "Person");
        var old = old.replace(/first_air_date/g, "release_date");
        var old = old.replace(/profile_path/g, "poster_path");
        var old = old.replace(/null/g, '"/9lfT4FfPtwpu0k0MTZqFchvQNsR.jpg"');
        this.newArray = JSON.parse(old);
        var i;
        for (i = 0; i < this.newArray.length; i++) {
          if (this.newArray[i] != 200) {
            this.newArray[i].release_date = this.newArray[i].release_date.substring(0, 4);
          }
        }
      }, err => {
        console.log(err);
      });

  }
  open(event, item) {
    event.stopPropagation();
    this.storage.set('idDisplay', item.id);
    console.log(item.media_type);
    this.storage.set('typeDisplay', item.media_type);
    window.location.reload();
    this.rtr.navigateByUrl('/tabs/(about:about)');
  }
  ngOnInit() {
    this.number = 2;
    this.target = "a";
    this.http.get('https://api.themoviedb.org/3/search/multi?api_key=37ccb790eeea88e5d24a68672814040a&language=en-US&query=a&page=1&include_adult=false').map(res => res.json())
      .subscribe(data => {
        this.data = data.results;
        var old = JSON.stringify(this.data).replace(/original_title/g, "display");
        var old = old.replace(/original_name/g, "display");
        var old = old.replace(/name/g, "display");
        var old = old.replace(/tv/g, "Serie");
        var old = old.replace(/movie/g, "Movie");
        var old = old.replace(/person/g, "Person");
        var old = old.replace(/first_air_date/g, "release_date");
        var old = old.replace(/profile_path/g, "poster_path");
        var old = old.replace(/null/g, '"/9lfT4FfPtwpu0k0MTZqFchvQNsR.jpg"');
        this.newArray = JSON.parse(old);
        var i;
        for (i = 0; i < this.newArray.length; i++) {
          if (this.newArray[i] != 200) {
            this.newArray[i].release_date = this.newArray[i].release_date.substring(0, 4);
          }
        }
      }, err => {
        console.log(err);
      });
  }

  loadData(event) {
    setTimeout(() => {
      event.target.complete();
      this.http.get('https://api.themoviedb.org/3/search/multi?api_key=37ccb790eeea88e5d24a68672814040a&language=en-US&query=' + this.target + '&page=' + this.number + '&include_adult=false').map(res => res.json())
        .subscribe(data => {
          this.data = data.results;
          var old = JSON.stringify(this.data).replace(/original_title/g, "display");
          var old = old.replace(/original_name/g, "display");
          var old = old.replace(/name/g, "display");
          var old = old.replace(/tv/g, "Serie");
          var old = old.replace(/movie/g, "Movie");
          var old = old.replace(/person/g, "Person");
          var old = old.replace(/first_air_date/g, "release_date");
          var old = old.replace(/profile_path/g, "poster_path");
          var old = old.replace(/null/g, '"/9lfT4FfPtwpu0k0MTZqFchvQNsR.jpg"');
          this.newArrayLoad = (JSON.parse(old));
          var i;
          for (i = 0; i < this.newArrayLoad.length; i++) {
            if (this.newArrayLoad[i] != 200) {
              this.newArrayLoad[i].release_date = this.newArrayLoad[i].release_date.substring(0, 4);
              this.newArray.push(this.newArrayLoad[i]);
            }
          }
        }, err => {
          console.log(err);
        });
      this.number = this.number + 1;
    }, 500);
  }

}
